/* MAP */

$(document).ready(function() {
  initGoogleMap('#map');
});

function initGoogleMap(name) {
  var elem = $(name);

  if(elem.length > 0) {
    var contactsMap;
    var ourCoords = new google.maps.LatLng(50.439562,30.521229);
    var mapCenterCoords = new google.maps.LatLng(50.439562,30.519229);
    var contactsMapOptions = {
      zoom: 16, 
      center: mapCenterCoords,
      mapTypeControl: false,
      scrollwheel: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    contactsMap = new google.maps.Map(elem[0], contactsMapOptions);

    var ourMarker = new google.maps.Marker({ 
      position: ourCoords,
      map: contactsMap,
      title: 'Юридическая компания "Алексеев, Боярчуков и партнеры"'
    });
  }
}

/* TABS */

$(document).ready(function() {
  initTabs('#practice-tabs');
});

function initTabs(name) {
  var elem = $(name);

  if(elem.length > 0) {
    if(elem.length > 0) {
      elem.find('a:first').tab('show');

      elem.find('a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
      });
    }
  }
}

/* SLIDER */

$(document).ready(function() {
  initSlider('#slider-events', 'banner');
});

function initSlider(name, mode) {
  var elem = $(name);

  if(elem.length > 0) {
    if(mode == 'banner') {
      elem.bxSlider({
        pager: true,
        controls: false,
        speed: 1250,
        auto: true,
        pause: 10000
      });
    } else {

    }
  }
}
